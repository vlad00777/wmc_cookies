jQuery(function($) {
    var el = $('[data-cookies]');

    $('body').on('click', '[data-closecookies]', function () {
        var inAnim = el.data("openanim");
        el.removeClass(inAnim);

        var out = el.data("closeanim");
        el.addClass(out);

        Cookies.set('wmcc_cookies', '1', {expires: 12});
    });

    var myCookie = Cookies.get('wmcc_cookies');
    if (myCookie === undefined) {
        el.show();
        var inAnim = el.data("openanim");
        el.addClass(inAnim);
    }
});