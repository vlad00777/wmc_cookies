<?php
/**
* Plugin Name: Wordpress Max Custom Cookies
* Plugin URI: https://vlad00777.pl
* Description: A max cutomize cookies plugin
* Version: 1.0
* Author: Vladislav Malienkov
* Author URI: vlad00777@gmail.com
*/


if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
  require_once __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
  require_once __DIR__ . '/CMB2/init.php';
}

require_once __DIR__ . '/cmb2-radio-image.php';
require_once __DIR__ . '/cmb2-field-animation.php';


add_action( 'cmb2_admin_init', 'wmcc_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function wmcc_register_theme_options_metabox() {

	$prefix = '_wmcc_';
	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'wmcc_option_metabox',
		'title'        => esc_html__( 'Wordpress MAX Customize Cookies', 'wmcc' ),
		'object_types' => array( 'options-page' ),
		'option_key'      => 'wmcc_options', // The option key and admin menu page slug.
		 'icon_url'        => 'dashicons-archive', // Menu icon. Only applicable if 'parent_slug' is left empty.
		 'menu_title'      => esc_html__( 'WMC Cookies', 'wmcc' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'wmcc' ), // The text for the options-page save button. Defaults to 'Save'.
	) );
	
	$cmb_options->add_field( array(
		'name'    => __( 'Cookies background color', 'wmcc' ),
		'desc'    => __( 'Choose cookies background color', 'wmcc' ),
		'id'      => $prefix .'bg_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#bada55',
	) );
	$cmb_options->add_field( array(
		'name'    => __( 'Cookies text color', 'wmcc' ),
		'desc'    => __( 'Choose cookies text color', 'wmcc' ),
		'id'      => $prefix .'color_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#ffffff',
	) );
	$cmb_options->add_field( array(
		'name'    => __( 'Cookies link color', 'wmcc' ),
		'desc'    => __( 'Choose cookies link color', 'wmcc' ),
		'id'      => $prefix .'link_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#bada55',
	) );
	$cmb_options->add_field( array(
		'name'    => __( 'Cookies link mouseover color', 'wmcc' ),
		'desc'    => __( 'Choose cookies link mouseover color', 'wmcc' ),
		'id'      => $prefix .'linkHover_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#bada55',
	) );

	$cmb_options->add_field( array(
		'name'    => __( 'Cookies close color', 'wmcc' ),
		'desc'    => __( 'Choose cookies close icon color', 'wmcc' ),
		'id'      => $prefix .'close_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#ffffff',
	) );

	$cmb_options->add_field( array(
		'name'    => __( 'Cookies close mouseover color', 'wmcc' ),
		'desc'    => __( 'Choose cookies close mouseover color', 'wmcc' ),
		'id'      => $prefix .'closeHover_colorpicker',
		'type'    => 'colorpicker',
		'default' => '#cccccc',
	) );
	
	$cmb_options->add_field( array(
		'name'          => __( 'In animation', 'wmcc' ),
		'desc'          => __( 'Select in animation', 'wmcc' ),
		'id'            => $prefix .'in_animation',
		'type'          => 'animation',
		'preview'       => true
	) );
	
	$cmb_options->add_field( array(
		'name'          => __( 'Out animation', 'wmcc' ),
		'desc'          => __( 'Select out animation', 'wmcc' ),
		'id'            => $prefix .'out_animation',
		'type'          => 'animation',
		'preview'       => true
	) );
	
	$cmb_options->add_field( array(
		'name'    => 'Cookies block position',
		'id'      => $prefix .'cookieBlockPosition_radio_inline',
		'type'    => 'radio_inline',
		'options' => array(
			'top' => __( 'Top', 'wmcc' ),
			'bottom'   => __( 'Bottom', 'wmcc' ),
		),
		'default' => 'bottom',
	) );
	
	$cmb_options->add_field( array(
		'name' => 'Enable icon?',
		'desc' => 'Enable icon in leftside or rightside?',
		'id'   => $prefix .'icon_checkbox',
		'type' => 'checkbox',
	) );
	
	$cmb_options->add_field( array(
		'name'             => __( 'Select cookies icon', 'wmcc' ),
		'desc'             => __( 'Select cookies icon type', 'wmcc' ),
		'id'               => $prefix .'radioimg',
		'type'             => 'radio_image',
		'options'          => array(
			'colorIcon'    => __('Color icon', 'wmcc'),
			'whiteIcon'  => __('White icon', 'wmcc'),
			'darkIcon'  => __('Dark icon', 'wmcc'),
			'fullIcon'  => __('Full icon', 'wmcc'),
			'lightIcon'  => __('Light icon', 'wmcc'),
		),
		'images_path'      => plugin_dir_url( __FILE__ ),
		'images'           => array(
			'colorIcon'    => 'images/cookie.png',
			'whiteIcon'  => 'images/cookies.png',
			'darkIcon'  => 'images/cookie2.png',
			'fullIcon'  => 'images/cookie3.png',
			'lightIcon'  => 'images/cookie4.png',
		),
		'default' => 'colorIcon',
	) );
	
	$cmb_options->add_field( array(
		'name'    => 'Cookies icon position',
		'id'      => $prefix .'cookiePosition_radio_inline',
		'type'    => 'radio_inline',
		'options' => array(
			'left' => __( 'Left', 'wmcc' ),
			'right'   => __( 'Right', 'wmcc' ),
		),
		'default' => 'left',
	) );
	
	$cmb_options->add_field( array(
		'name'    => 'Cookies text',
		'desc'    => 'Enter cookie text',
		'id'      => $prefix .'cookieText_wysiwyg',
		'type'    => 'wysiwyg',
		'options' => array(),
		'default' => 'The website uses cookies in accordance with the <a href="https://obslugaprawna.pro/en/cookies-policy/" target="_blank" rel="noopener">Policy cookies </a>and <a href="https://obslugaprawna.pro/en/privacy-policy/" target="_blank" rel="noopener">Privacy Policy</a>.
		At any time, you can specify the conditions for storage or access cookies on your browser, or configure services.'
	) );
}


function wmcc_get_option( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		return cmb2_get_option( 'wmcc_options', $key, $default );
	}
	$opts = get_option( 'wmcc_options', $default );
	$val = $default;
	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}
	return $val;
}

function your_function() {
	$bg = wmcc_get_option('_wmcc_bg_colorpicker');
	$text = wmcc_get_option('_wmcc_cookieText_wysiwyg');
	$color = wmcc_get_option('_wmcc_color_colorpicker');
	$colorLink = wmcc_get_option('_wmcc_link_colorpicker');
	$colorLinkHover = wmcc_get_option('_wmcc_linkHover_colorpicker');
	$inAnimation = wmcc_get_option('_wmcc_in_animation');
	$outAnimation = wmcc_get_option('_wmcc_out_animation');
	$position = wmcc_get_option('_wmcc_cookieBlockPosition_radio_inline');
	$positionIcon = wmcc_get_option('_wmcc_cookiePosition_radio_inline');
	$icon = wmcc_get_option('_wmcc_icon_checkbox');
	$iconImg = wmcc_get_option('_wmcc_radioimg');
	$close = wmcc_get_option('_wmcc_close_colorpicker');
	$closeHover = wmcc_get_option('_wmcc_closeHover_colorpicker');
	echo "
	<style>
		.cookiesInfo{
			background-color:".$bg.";
		}
		.cookiesInfo__text {
			color:".$color.";
		}
		.cookiesInfo__text a{
			color:".$colorLink.";
		}
		.cookiesInfo__text a:hover{
			color:".$colorLinkHover.";
		}
		.cookiesInfo__close{
			background-color:".$bg.";
		}
		.cookiesInfo__close svg{
			fill:".$close.";
		}
		.cookiesInfo__close:hover svg{
			fill:".$closeHover.";
		}
	</style>";

	echo 
	'<div class="cookiesInfo animated cookiesInfo--'.$position.' cookiesInfo--'.$positionIcon.'" data-cookies="data-cookies" data-closeAnim="'.$outAnimation.'" data-openAnim="'.$inAnimation.'">
		<div class="cookiesInfo__inner">';
	if($icon) {
		echo '<div class="cookiesInfo__icon cookiesInfo__icon--'.$iconImg.'"></div>';
	}
	echo 
	'<div class="cookiesInfo__text">'.$text.'</div>
		<div class="cookiesInfo__close" data-closecookies="data-closecookies"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
		<path d="M505.943 6.058c-8.077-8.077-21.172-8.077-29.249 0L6.058 476.693c-8.077 8.077-8.077 21.172 0 29.249A20.612 20.612 0 0 0 20.683 512a20.614 20.614 0 0 0 14.625-6.059L505.943 35.306c8.076-8.076 8.076-21.171 0-29.248z"/>
		<path d="M505.942 476.694L35.306 6.059c-8.076-8.077-21.172-8.077-29.248 0-8.077 8.076-8.077 21.171 0 29.248l470.636 470.636a20.616 20.616 0 0 0 14.625 6.058 20.615 20.615 0 0 0 14.624-6.057c8.075-8.078 8.075-21.173-.001-29.25z"/>
	  </svg></div>
		</div>
	</div>';
	
}
add_action( 'wp_footer', 'your_function', 100 );


function wmcc_styles() {
    wp_enqueue_style( 'style', plugin_dir_url( __FILE__ ) . 'css/style.css', array(), '1.0.1', false );
	wp_enqueue_style( 'animate', plugin_dir_url( __FILE__ ) . 'css/animate.css', array(), '1.0.0', false );
	
	wp_enqueue_script( 'jscookie', plugin_dir_url( __FILE__ ) . 'js/js.cookie.js');
	wp_enqueue_script( 'cookie', plugin_dir_url( __FILE__ ) . 'js/cookies.js');
	
	
}
add_action( 'wp_enqueue_scripts', 'wmcc_styles', 100 );




function wmcc_admin() {
    wp_enqueue_style( 'admin', plugin_dir_url( __FILE__ ) . 'css/admin.css', array(), '1.0.0', false );
	
	
}
add_action( 'admin_enqueue_scripts', 'wmcc_admin', 100 );